var dssv =[];
var BASE_UL = "https://62f8b754e0564480352bf3c3.mockapi.io";
var batLoading = function () {
  document.getElementById("loading").style.display="flex";
}
var tatLoading = function () {
  document.getElementById("loading").style.display="none";
}


var renderTable = function (list) {
  var contentHTML = "";

  list.forEach(function(item) {
    var trContent = `
    <tr>
    <td>${item.ma}</td>
    <td>${item.ten}</td>
    <td>${item.email}</td>
    <td>
    <img src=${item.hinhAnh} style = "width:80px" alt=""/>
    </td>
    <td><button onclick="xoaSinhVien('${item.ma}')" type="button" name="" id="" class="btn btn-danger" btn-lg btn-block">Xóa</button>
    <button onclick="suaSinhVien('${item.ma}')" type="button" name="" id="" class="btn btn-warning" btn-lg btn-block">Sửa</button></td>
    


    </tr>
    `;
    contentHTML += trContent;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

var renderDssvService = function() {
  batLoading();

  axios ({
    url: `${BASE_UL}/sv`,
    method: "GET",
   })
   .then (function(res) {
    tatLoading();

    dssv = res.data;
    
    renderTable(dssv);
   
  
   })
   .catch (function(err) {
    tatLoading();

    console.log('err: ', err);
  
   });
}

renderDssvService();

 function xoaSinhVien(id) {

  batLoading();
  axios ({
    url: `${BASE_UL}/sv/${id}`,
    method: "DELETE",
  })
  .then(function(res){
    tatLoading();
    renderDssvService();
    console.log('res: ', res);

  })
  .catch(function(err){
    tatLoading();

    console.log('err: ', err);

  });

 }


 function themSV() {
  var dataForm = layThongTinTuForm();
  batLoading();
  axios ({
    url: `${BASE_UL}/sv`,
    method: "POST",
    data: dataForm,
  })
  .then(function(res){
    tatLoading();
    renderDssvService();
    console.log('res: ', res);

  })
  .catch(function(err){
    tatLoading();

    console.log('err: ', err);

  });
 }


 function suaSinhVien(id) {
  batLoading();

  axios ({
    url: `${BASE_UL}/sv/${id}`,
    method: "GET",
  })
  .then(function(res){
    tatLoading();

    showThongTinLenForm(res.data);
    console.log('res: ', res);

  })
  .catch(function(err){
    tatLoading();

    console.log('err: ', err);
  });

 }

 function capNhatSV() {
   var dataForm = layThongTinTuForm();
   console.log('dataForm: ', dataForm);

   axios ({
    url: `${BASE_UL}/sv/${dataForm.ma}`,
    method: "PUT",
    data: dataForm,
    
  })
  .then(function(res){
    tatLoading();
    renderDssvService();
    console.log('res: ', res);

  })
  .catch(function(err){
    tatLoading();

    console.log('err: ', err);
  });
 }