function layThongTinTuForm () {
    var ten = document.getElementById("txtTenSV").value;
    var email = document.getElementById("txtEmail").value;
    var hinhAnh = document.getElementById("txtHinhAnh").value;
    var ma = document.getElementById("txtMaSV").value;
    return {
        ten: ten,
        email: email,
        hinhAnh: hinhAnh,
        ma: ma,
    };

}

function showThongTinLenForm(data) {
    document.getElementById("txtTenSV").value = data.ten;
    document.getElementById("txtEmail").value = data.email;
    document.getElementById("txtHinhAnh").value = data.hinhAnh;
    document.getElementById("txtMaSV").value = data.ma;


}